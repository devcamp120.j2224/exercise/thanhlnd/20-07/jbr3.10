package com.devcamp.rainbow;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowRestApi {
    @CrossOrigin
    @GetMapping("/rainbow")
        public ArrayList<String> Rainbow(){
            ArrayList<String> rainbowList = new ArrayList<String>();
            rainbowList.add("red");
            rainbowList.add("orange");
            rainbowList.add("yellow");
            rainbowList.add("green");
            rainbowList.add("blue");
            rainbowList.add("indigo");
            rainbowList.add("violet");
            return rainbowList;
        }
}
